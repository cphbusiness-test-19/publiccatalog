FROM ubuntu:latest

RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev build-essential
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
EXPOSE 5000
ENV FLASK_ENV=development
ENV FLASK_APP=temp.py
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
#CMD flask run
CMD python3 temp.py
