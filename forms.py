from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, TextField, SelectField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError

class courselist():
    course = [{'id':'1', 
           'courseName':'Database',
           'room':'262',
           'teacherId':'1',
           'student':'1',
           'category':'1',
           'noHourPerWeek':'3',
           'teachingTime':'8.30',
           'description':'bla bla mysql',
           'semester':'1',
           'language':'dansk',
           'level':'1',
           'visible':'0',
           'archived':'0'           
           },{
           'id':'2', 
           'courseName':'Test',
           'room':'203',
           'teacherId':'3',
           'student':'2',
           'category':'1',
           'noHourPerWeek':'6',
           'teachingTime':'14.00',
           'description':'test bla bla',
           'semester':'1',
           'language':'polsk',
           'level':'2',
           'visible':'1',
           'archived':'0' },
            {
           'id':'3', 
           'courseName':'Machine learning',
           'room':'150',
           'teacherId':'5',
           'student':'3',
           'category':'4',
           'noHourPerWeek':'3',
           'teachingTime':'12.00',
           'description':'ML bla bla',
           'semester':'1',
           'language':'dansk',
           'level':'2',
           'visible':'0',
           'archived':'0'
           },
             {
           'id':'4', 
           'courseName':'Fundamental programming',
           'room':'100',
           'teacherId':'3',
           'student':'6',
           'category':'1',
           'noHourPerWeek':'3',
           'teachingTime':'12.00, 14.00',
           'description':'bla bla',
           'semester':'2',
           'language':'dansk',
           'level':'1',
           'visible':'0',
           'archived':'0'
           },
              {
           'id':'5', 
           'courseName':'management control and finance',
           'room':'160',
           'teacherId':'8',
           'student':'12',
           'category':'2',
           'noHourPerWeek':'3',
           'teachingTime':'12.00, 14.00',
           'description':'bla bla',
           'semester':'1',
           'language':'English',
           'level':'1',
           'visible':'0',
           'archived':'0'
           },
            {
            'id':'6', 
           'courseName':'marketing strategy',
           'room':'168',
           'teacherId':'18',
           'student':'18',
           'category':'3',
           'noHourPerWeek':'3',
           'teachingTime':'8.00, 14.00',
           'description':'bla bla',
           'semester':'1',
           'language':'English',
           'level':'1',
           'visible':'0',
           'archived':'0'
           }, {
            'id':'7', 
           'courseName':'management accounting',
           'room':'240',
           'teacherId':'9',
           'student':'21',
           'category':'2',
           'noHourPerWeek':'3',
           'teachingTime':'8.00, 12.00',
           'description':'bla bla',
           'semester':'1',
           'language':'Polish',
           'level':'1',
           'visible':'0',
           'archived':'0'
           }
            
            ]

class category():
    categorylist = [{'id':'1', 
           'categoryName':'Software development'                   
           },{
           'id':'2', 
           'categoryName':'Management'},
           {
           'id':'3', 
           'categoryName':'Marketing'
           },
             {
           'id':'4', 
           'categoryName':'Data science'          
           }]

    



class ContactForm(FlaskForm):
       
   c = courselist()
   course = c.course
   
   
   Select = SelectField("SelectCourse", choices = course , validators=[DataRequired])
   name = TextField('name', validators=[DataRequired,Length(min=1, max=20)])
   phone = TextField('phone', validators=[DataRequired,Length(min=1, max=50)])
   email = StringField('Email', validators=[DataRequired,Length(min=1, max=50), Email()])
   
   submit = SubmitField('signup')
   
   
class ShowCourse(FlaskForm):
    
    c = courselist()
    course = c.course
    
    ca = category()
    category = ca.categorylist       
    
    coursedropdown = SelectField("SelectCourse", choices = course, validators=[DataRequired])
    categorydropdown = SelectField("Selectcategory", choices = category, validators=[DataRequired])
    
    submit = SubmitField('See details')


class courses():
    
    id=""
    courseName=""
    room=""
    teacherId=""
    student="",
    category=""
    noHourPerWeek=""
    teachingTime=""
    description=""
    semester=""
    language=""
    level=""
    visible=""
    archived=""  
   
    '''
    def __init__(self,id,courseName,room,teacherId,student,category,noHourPerWeek,teachingTime,description,semester,language,level, visible,archived):
        self.courseName = courseName
        self.room = room
        self.teacherId= teacherId
        self.student = student
        self.category = category
        self.noHourPerWeek = noHourPerWeek
        self.teachingTime=teachingTime
        self.description = description
        self.semester=semester
        self.language=language
        self.level=level
        self.visible=visible
        self.archived = archived
    '''
        
    def __init__(self,id,courseName,room,teacherId,category,noHourPerWeek,teachingTime,description,semester,language,level):
        self.id = id
        self.courseName = courseName
        self.room = room
        self.teacherId= teacherId
        
        self.category = category
        self.noHourPerWeek = noHourPerWeek
        self.teachingTime=teachingTime
        self.description = description
        self.semester=semester
        self.language=language
        self.level=level
      
        
    def showcourseid(self):
        return self.id 
        
    def showcoursname(self):
        return self.courseName    
    
    def showroom(self):
        return self.room    
    
    def showteacherId(self):
        return self.teacherId
    
    def showstudent(self):
        return self.student
    
    def showcategory(self):
        return self.category
    
    def shownoHourPerWeek(self):
        return self.noHourPerWeek
    
    def showteachingTime(self):
        return self.teachingTime
    
    def showdescription(self):
        return self.description
    
    def showsemester(self):
        return self.semester
    
    def showlanguage(self):
        return self.language
    
    def showlevel(self):
        return self.level
    
    def showvisible(self):
        return self.visible    
    
    def showarchived(self):
        return self.archived
    
    