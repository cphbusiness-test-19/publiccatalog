from unittest.mock import Mock, patch
from nose.tools import assert_true, assert_is_not_none, assert_list_equal, assert_is_none
import requests
from http.server import BaseHTTPRequestHandler, HTTPServer
import socket
from threading import Thread
from forms import ContactForm, ShowCourse, category, courselist
from datalayer import setURL, get_info, get_URL, get_URL_byID

class MockServerRequest(BaseHTTPRequestHandler):

    def test_response(self):
        #response = requests.get('http://127.0.0.1:5000/signup')
        response = requests.get('https://google.dk')
        assert_true(response.ok)

    def prccess_http_request(self):
        self.send_response(requests.codes.ok)
        self.end_headers()
        return


