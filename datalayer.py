import requests

url =""

def setURL(url):
    return url

def get_URL(url):
    return requests.get(setURL(url))

def get_URL_byID(task_id):
    return requests.get(setURL(url/'{task_id}/'.format(task_id)))

def get_info():
    return requests.get(setURL(url), json={
        'Id' : 'string',
        'CourseName' : 'string',
        'Room' : 'string',
        'TeacherId' : 'number',
        'Students' : '[number]',
        'Category' : 'string',
        'NoHoursPrWeek' : 'number',
        'Weekdays' : 'string',
        'Description' : 'string',
        'Semester' : 'string',
        'Language' : 'string',
        'Level' : 'string',
        'Visible' : 'bool',
        'Archived' : 'bool'
    })


