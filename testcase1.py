from flask import request
import unittest
import temp
from forms import ContactForm, ShowCourse, category, courselist


class TestPage(unittest.TestCase):
    
   
    def setUp(self):
        tester = temp.app.test_client(self)
        response = tester.get('/')        
        print(response.status_code)
        
    
    # testing the setup of flask
    def testMain(self):
        tester = temp.app.test_client(self)
        response = tester.get('/coursedescription/1', content_type='html/text')
        #self.assertEqual(response.status_code, 301)
        self.assertEqual(response.status_code, 301)
        print('testMain', response.status_code)
    
        
    
    def testTextMain(self):
        tester = temp.app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertTrue(b'Welcome to Lyngby Evening School' in response.data)
        print('testTextMain')

      

    def testTextCoursedescription(self):
        
        #temp.app.testing = True
        tester = temp.app.test_client(self)
        response = tester.get('coursedescription/1/', content_type='html/text')
        self.assertTrue(b'Course Description' in response.data)
        print('testTextCoursedescription')


    def testObj(self):
        
        with temp.app.test_request_context('/'):
            s = ShowCourse()
            self.a = s.categorydropdown.choices = 1
            self.assertEqual(self.a,1)
            print('testObj')
    
    
    # formular
    def testformularSelect(self):
        
        with temp.app.test_request_context('/signup'):
            s = ContactForm()
            a = s.Select.choices = 1
            self.assertEqual(a,1)
            print('testformularSelect')
            

    def testformularInput(self):
        
        with temp.app.test_request_context('/signup'):
            s = ContactForm()
            course = s.Select.choices = 0
            name = s.name = 'john'
            email = s.email = ''
            phone = s.phone = '12345678'
            
            self.assertEqual(course,0)
            self.assertEqual(name,'john')
            self.assertEqual(email,'')
            self.assertEqual(phone,'12345678')
            
            
            
            print('testformularInput')
    


if __name__ == '__main__':
    unittest.main()



