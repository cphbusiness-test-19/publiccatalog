from flask import Flask, render_template, request, url_for, flash, redirect, jsonify
from forms import ContactForm, ShowCourse, category



#from flask_sqlalchemy import SQLAlchemy
#from .forms import ContactForm, ShowCourse, category
#from forms import *

app = Flask(__name__)


app.config['SECRET_KEY'] = 'xe0gtke90f4k3'
#app.config['SQL_ALCHEMY_URI'] = 'sqllite///site.db'

#db = SQLAlchemy(app)


#@app.route("/")
@app.route("/")
def index():
    forms = ShowCourse()
       
    return render_template('front.html', forms=forms)   


@app.route("/about")
def about():
    return render_template('about.html', title='about')


@app.route("/coursedescription/<string:id>/")
def coursedescription(id):
       
    forms = ShowCourse()
    return render_template('coursedescription.html', title='description', forms=forms, id=id)



@app.route("/signup", methods=['POST','GET'])
def signup():
    forms = ContactForm()
           
    return render_template('signup.html', title='signup', forms=forms)


@app.route("/success", methods=['POST'])
def success():
    forms = ContactForm()
    if request.method == 'POST':
        selid = request.form['selectid']
        
        name = request.form['name']
        phone = request.form['phone']
        email = request.form['email']
        
        return render_template('success.html', title='success', forms=forms, selid=selid,name=name, phone=phone, email=email)
        
    return render_template('success.html', title='success', forms=forms)


if __name__ =='__main__':
    #app.run(debug=True)
    app.run(host="0.0.0.0", port=5000)